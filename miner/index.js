const Transaction = require('../wallet/transaction')
const Wallet = require('../wallet')
const { BLOCK_MAX_TRANSACTIONS, } = require('../config')


class Miner {
	constructor(blockchain, transactionPool, wallet, p2pServer) {
		this.blockchain = blockchain
		this.transactionPool = transactionPool
		this.wallet = wallet
		this.p2pServer = p2pServer
	}

	static transactionsToMine(validTransactions) {
		let transactionsToMine = validTransactions.sort((first, second) =>  Transaction.totalTransactionFee(first) < Transaction.totalTransactionFee(second))

		return transactionsToMine.slice(0, BLOCK_MAX_TRANSACTIONS)
	}

	mine() {
		const validTransactions = this.transactionPool.validTransactions()
		const transactionsToMine = Miner.transactionsToMine(validTransactions)

		transactionsToMine.push(
			Transaction.rewardTransaction(this.wallet, Wallet.blockchainWallet(), transactionsToMine)
		)
		
		const block = this.blockchain.addBlock(transactionsToMine)
		this.p2pServer.syncChains()
		this.transactionPool.clear(transactionsToMine)
		this.p2pServer.broadcastClearTransactions(transactionsToMine)
        
		return block
	}
}

module.exports = Miner