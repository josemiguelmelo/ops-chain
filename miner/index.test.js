const Miner = require('./index')
const Wallet = require('../wallet')
const TransactionPool = require('../wallet/transaction-pool')
const Blockchain = require('../blockchain')

const { BLOCK_MAX_TRANSACTIONS, MAXIMUM_FEE_PERCENT, } = require('../config')

describe('Miner', () => {
    let frsWallet, scnWallet, trdWallet
    let tp, bc, fee, highestFee, sendAmount, recipient

    beforeEach(() => {
        frsWallet = new Wallet()
        scnWallet = new Wallet()
        trdWallet = new Wallet()
        tp = new TransactionPool()
        bc = new Blockchain()
        
        sendAmount = 50
        fee = sendAmount * MAXIMUM_FEE_PERCENT - 0.3
        highestFee = sendAmount * MAXIMUM_FEE_PERCENT
        recipient = 'r3c1p13nt3'
        
        frsWallet.createTransaction(recipient, sendAmount, bc, tp, fee)
        transaction = scnWallet.createTransaction(recipient, sendAmount, bc, tp, highestFee)
        trdWallet.createTransaction(recipient, sendAmount, bc, tp, fee)
    })

    it('must only return 1 transaction to mine', () => {
        let transactionsToMine = Miner.transactionsToMine(tp.validTransactions())
        expect(transactionsToMine.length).toEqual(BLOCK_MAX_TRANSACTIONS)
    })

    it('must return highest fee transaction', () => {
        let transactionsToMine = Miner.transactionsToMine(tp.validTransactions())
        expect(transactionsToMine[0]).toEqual(transaction)
    })

})