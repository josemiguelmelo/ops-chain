const Transaction = require('./transaction')

class TransactionPool {
	constructor() {
		this.transactions = []
	}

	updateOrAddTransaction(transaction) {
		let transactionWithId = this.transactions.find(t => t.id === transaction.id)
        
		if (transactionWithId) {
			this.transactions[this.transactions.indexOf(transactionWithId)] = transaction
		}
		else {
			this.transactions.push(transaction)
		}
	}

	existingTransaction(address) {
		return this.transactions.find(t => t.input.address === address)
	}
    
	validTransactions() {
		return this.transactions.filter(transaction => {
			const outputTotal = transaction.outputs.reduce((total, output) => {
				let fee = output.fee ? output.fee : 0
				return total + output.amount + fee
			}, 0)
            
			if (transaction.input.amount !== outputTotal) return console.log(`Invalid transaction from ${transaction.input.address}`)

			if (! Transaction.verifyTransaction(transaction)) return console.log(`Invalid signature from ${transaction.input.address}`)

			return transaction
		})
	}
    
	clear(transactions = null) {
		if (transactions === null) return this.transactions = []

		this.transactions = this.transactions.filter(e => {
			for (var i = 0; i < transactions.length; i++) 
				if (transactions[i].id === e.id) return false
			return true
		})
	}
}

module.exports = TransactionPool