const Wallet = require('./index')
const TransactionPool = require('./transaction-pool')
const Blockchain = require('../blockchain')

const { INITIAL_BALANCE, } = require('../config')

describe('Wallet', () => {
    let wallet, tp, bc, fee

    beforeEach(() => {
        wallet = new Wallet()
        tp = new TransactionPool()
        bc = new Blockchain()
        fee = 3
    })

    describe('creating a transaction with fee lower than minimum', () => {
        let lowMinimumFee, sendAmount, recipient

        beforeEach(() => {
            lowMinimumFee = 0.2
            sendAmount = 50
            recipient = 'r3c1p13nt3'
        })

        it('must not create transaction', () => {
            expect(wallet.createTransaction.bind(recipient, sendAmount, bc, tp, lowMinimumFee)).toThrow(Error)
        })
    })

    describe('creating a transaction', () => {
        let transaction, sendAmount, recipient

        beforeEach(() => {
            sendAmount = 50
            recipient = 'r3c1p13nt3'
            transaction = wallet.createTransaction(recipient, sendAmount, bc, tp, fee)
        })

        describe('doing the same transaction', () => {
            beforeEach(() => {
                wallet.createTransaction(recipient, sendAmount, bc, tp, fee)
            })

            it('doubles the `sendAmount` subtracted from the wallet balance', () => {
                expect(transaction.outputs.find(output => output.address === wallet.publicKey).amount)
                    .toEqual(wallet.balance - sendAmount * 2)
            })

            it('clones the `sendAmount` output for the recipient', () => {
                expect(transaction.outputs.filter(output => output.address === recipient)
                    .map(output => output.amount)).toEqual([sendAmount - fee, sendAmount - fee])
            })
        })
    })

    describe('calculating a balance', () => {
        let addBalance, repeatAdd, senderWallet

        beforeEach(() => {
            senderWallet = new Wallet()
            addBalance = 100
            repeatAdd = 3

            for (let i = 0; i < repeatAdd; i++) {
                senderWallet.createTransaction(wallet.publicKey, addBalance, bc, tp, fee)
            }

            bc.addBlock(tp.transactions)
        })

        it('calculates the balance for the recipient', () => {
            expect(wallet.calculateBalance(bc)).toEqual(INITIAL_BALANCE + ((addBalance - fee) * repeatAdd))
        })

        it('calculates the balance for the sender', () => {
            expect(senderWallet.calculateBalance(bc)).toEqual(INITIAL_BALANCE - (addBalance * repeatAdd))
        })

        describe('and the recipient makes a transaction', () => {
            let subtractBalance, recipientBalance

            beforeEach(() => {
                tp.clear()
                subtractBalance = 60
                recipientBalance = wallet.calculateBalance(bc)

                wallet.createTransaction(senderWallet.publicKey, subtractBalance, bc, tp, fee)
                
                bc.addBlock(tp.transactions)
            })

            describe('sender sends another transaction to the recipient', () => {
                beforeEach(() => {
                    tp.clear()    
                    senderWallet.createTransaction(wallet.publicKey, addBalance, bc, tp, fee)
                    bc.addBlock(tp.transactions)
                })
    
                it('calculates the recipient balance but only using transactions since its most recent one', () => {
                    expect(wallet.calculateBalance(bc)).toEqual(recipientBalance - subtractBalance + (addBalance - fee))
                })
            })
        })
    })

    describe('loading wallet from private key', () => {
        let walletTemp, recipient, sendAmount, transaction, privateKey

        beforeEach(() => {
            privateKey = wallet.keyPair.getPrivate()
            sendAmount = 50
            recipient = 'r3c1p13nt3'
            walletTemp = Wallet.walletFromPrivateKey(privateKey)
            transaction = walletTemp.createTransaction(recipient, sendAmount, bc, tp, fee)
        })

        it('must get the wallet for private key', () => {
            let walletTempPrivateKey = walletTemp.keyPair.getPrivate().toString('hex')
            let walletTempPublicKey = walletTemp.keyPair.getPublic().toString('hex')

            expect(walletTempPrivateKey).toEqual(privateKey.toString('hex'))
            expect(walletTempPublicKey).toEqual(wallet.keyPair.getPublic().toString('hex'))
            expect(walletTemp.publicKey).toEqual(wallet.publicKey.toString())
        })

        it('must be able to create transaction with wallet from key pair', () => {
            expect(transaction.outputs.find(output => output.address === wallet.publicKey).amount)
                    .toEqual(wallet.balance - sendAmount)
        })

    })
})