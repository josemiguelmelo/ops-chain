const Transaction = require('./transaction')
const Wallet = require('./index')
const { MINING_REWARD, MAXIMUM_FEE_PERCENT } = require('../config')

describe('Transaction', () => {
    let transaction, wallet, recipient, amount, fee

	beforeEach(() => {
        wallet = new Wallet()
        amount = 50
        recipient = 'r3c1p13nt'
        fee = 1
        transaction = Transaction.newTransaction(wallet, recipient, amount, fee)
    })
    
    it('outputs the `amount` subtracted from the wallet balance', () => {
        expect(transaction.outputs.find(output => output.address === wallet.publicKey).amount)
            .toEqual(wallet.balance - amount)
    })
    
    it('outputs the `amount` added to the recipient', () => {
        expect(transaction.outputs.find(output => output.address === recipient).amount)
            .toEqual(amount - fee)
    })
    
    it('inputs the balance of the wallet', () => {
        expect(transaction.input.amount).toEqual(wallet.balance)
    })

    it('validates a valid transaction', () => {
        expect(Transaction.verifyTransaction(transaction)).toBe(true)
    })

    it('invalidates a corrupt transaction', () => {
        transaction.outputs[0].amount = 10000
        expect(Transaction.verifyTransaction(transaction)).toBe(false)
    })

    describe('transacting with an amount that exceeds the balance', () => {
        beforeEach(() => {
            amount = 50000
        })

        it('does not create the transaction', () => {
            expect(Transaction.newTransaction.bind(wallet, recipient, amount, fee)).toThrow(Error)
        })
    })

    describe('transacting with fee greater than max fee for transaction', () => {
        beforeEach(() => {
            amount = 50
        })
        
        it('does not create the transaction', () => {
            let maxFee = amount * MAXIMUM_FEE_PERCENT
            expect(Transaction.newTransaction.bind(wallet, recipient, amount, maxFee + 0.1)).toThrow(Error)
        })
    })

    describe('updating a transaction', () => {
        let nextAmount, nextRecipient

        beforeEach(() => {
            nextAmount = 20
            nextRecipient = 'n3xt-4ddr355'
            transaction = transaction.update(wallet, nextRecipient, nextAmount, fee)
        })

        it(`subtracts the next amount from the sender's output`, () => {
            expect(transaction.outputs.find(output => output.address === wallet.publicKey).amount)
                .toEqual(wallet.balance - amount - nextAmount)
        })

        it(`outputs an amount for the next recipient`, () => {
            expect(transaction.outputs.find(output => output.address === nextRecipient).amount)
                .toEqual(nextAmount - fee)
        })
    })

    describe('creating a reward transaction', () => {
        beforeEach( () => {
            transaction = Transaction.rewardTransaction(wallet, Wallet.blockchainWallet(), [transaction, transaction])
        })

        it(`reward the miner's wallet`, () => {
            expect(transaction.outputs.find(output => output.address === wallet.publicKey).amount)
                .toEqual(fee * 2)
        })
    })
})