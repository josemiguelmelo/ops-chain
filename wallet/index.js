const ChainUtil = require('../chain-util')
const Transaction = require('./transaction')
const { INITIAL_BALANCE, } = require('../config')

class Wallet {
	constructor() {
		this.balance = INITIAL_BALANCE
		this.keyPair = ChainUtil.genKeyPair()
		this.publicKey = this.keyPair.getPublic().encode('hex')
	}
        
	toString() {
		return `Wallet -
            privateKey: ${this.keyPair.getPrivate().toString('hex')}
            publicKey: ${this.publicKey.toString()}
            balance  : ${this.balance}`
	}
    
	sign(dataHash) {
		return this.keyPair.sign(dataHash)
	}
    
	createTransaction(recipient, amount, blockchain, transactionPool, fee) {
		this.balance = this.calculateBalance(blockchain)

		let hasErrors = Transaction.hasTransactionInformationErrors(this, recipient, amount, fee)
		if (hasErrors) throw hasErrors
		
		let transaction = transactionPool.existingTransaction(this.publicKey)

		if (transaction) {
			transaction.update(this, recipient, amount, fee)
		} else {
			transaction = Transaction.newTransaction(this, recipient, amount, fee)
			transactionPool.updateOrAddTransaction(transaction)
		}
        
		return transaction
	}

	calculateBalance(blockchain) {
		let balance = this.balance
		let transactions = []

		blockchain.chain.forEach(block => {
			block.data.forEach(transaction => {
				transactions.push(transaction)
			})
		})
        
		const walletInputTs = transactions
			.filter(transaction => transaction.input.address === this.publicKey)
            
		let startTime = 0

		if (walletInputTs.length > 0) {
			const recentInputT = walletInputTs.reduce((prev, current) => prev.input.timestamp > current.input.timestamp ? prev : current)

			balance = recentInputT.outputs.find(output => output.address === this.publicKey).amount
			startTime = recentInputT.input.timestamp
		}
        
		transactions.forEach(transaction => {
			if (transaction.input.timestamp > startTime) {
				transaction.outputs.find(output => {
					if (output.address === this.publicKey)
						balance += output.amount
				})
			}
		})

		return balance
	}
    
	static walletFromPrivateKey(privateKey) {
		let wallet = new Wallet()
		wallet.keyPair = ChainUtil.restoreKeyPair(privateKey)
		wallet.publicKey = wallet.keyPair.getPublic().encode('hex')
		return wallet
	}

	static blockchainWallet() {
		const blockchainWallet = new this()
		blockchainWallet.address = 'blockchain-wallet'
		return blockchainWallet
	}
}

module.exports = Wallet