const TransactionPool = require('./transaction-pool')
const Transaction = require('./transaction')
const Wallet = require('./index')
const Blockchain = require('../blockchain')


describe('TransactionPool', () => {
    let tp, wallet, transaction, bc, fee

    beforeEach(() => {
        tp = new TransactionPool()
        wallet = new Wallet()
        bc = new Blockchain()
        fee = 1
        transaction = Transaction.newTransaction(wallet, 'r3c1p13nt', 30, fee)
        tp.updateOrAddTransaction(transaction)
        transaction = wallet.createTransaction('r3c1p13nt', 30, bc, tp, fee)
    })

    it('adds a transaction to the pool', () => {
        expect(tp.transactions.find(t => t.id === transaction.id)).toEqual(transaction)
    })

    it('updates a transaction in the pool', () => {
        const oldTransaction = JSON.stringify(transaction)
        const newTransaction = transaction.update(wallet, 'n3w-r3c1p13nt', 40)

        tp.updateOrAddTransaction(newTransaction)

        expect(JSON.stringify(tp.transactions.find(t => t.id === transaction.id)))
            .not.toEqual(oldTransaction)
            
        expect(JSON.stringify(tp.transactions.find(t => t.id === transaction.id)))
            .toEqual(JSON.stringify(newTransaction))
    })

    it('clear transactions', () => {
        tp.clear()
        expect(tp.transactions).toEqual([])
    })

    it('clear specific transactions', () => {
        tp.clear([transaction])
        expect(tp.transactions).toEqual([])
    })

    describe('mixing valid and corrupt transactions', () => {
        let mixingTransactions

        beforeEach(() => {
            validTransactions = [...tp.transactions]

            for (let i = 0; i < 6; i++) {
                wallet = new Wallet()

                transaction = wallet.createTransaction('r4nd.4dre355', 30, bc, tp, fee)
 
                if (i%2 == 0) transaction.input.amount = 99999
                else validTransactions.push(transaction)
            }
        })

        it('shows a differece between valid and corrupt transactions', () => {
            expect(JSON.stringify(tp.transactions)).not.toEqual(JSON.stringify(validTransactions))
        })

        it('grabs valid transactions', () => {
            expect(tp.validTransactions()).toEqual(validTransactions)
        })
    })
    
})
