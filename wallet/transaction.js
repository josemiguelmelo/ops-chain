const ChainUtil = require('../chain-util')
const { MINIMUM_FEE_PERCENT, MAXIMUM_FEE_PERCENT, } = require('../config')

class Transaction {
	constructor() {
		this.id = ChainUtil.id()
		this.input = null
		this.outputs = []
	}
    
	update(senderWallet, recipient, amount, fee) {
		const senderOutput = this.outputs.find(output => output.address === senderWallet.publicKey)
        
		if (amount > senderOutput.amount) return console.log(`Amount: ${amount} exceeds balance.`)

		senderOutput.amount = senderOutput.amount - amount
		this.outputs.push({ amount: amount - fee, address: recipient, fee: fee, })
		Transaction.signTransaction(this, senderWallet)
        
		return this
	}

	static totalTransactionFee(transaction) {
		return transaction.outputs.reduce((total, output) => {
			if (transaction.input.address === output.address) return total
			let fee = output.fee ? output.fee : 0
			return total + fee
		}, 0)
	}

	static transactionWithOutputs(senderWallet, outputs, fee) {
		const transaction = new this()
		transaction.outputs.push(...outputs)
		Transaction.signTransaction(transaction, senderWallet, fee)
		return transaction
	}
    
	static hasTransactionInformationErrors(senderWallet, recipient, amount, fee) {
		if (fee < amount * MINIMUM_FEE_PERCENT) return new Error(`Minimum fee: ${(amount * MINIMUM_FEE_PERCENT)} is the minimum fee for this transaction.`)
		if (fee > amount * MAXIMUM_FEE_PERCENT) return new Error(`Maximum fee: ${(amount * MAXIMUM_FEE_PERCENT)} is the maximum fee for this transaction.`)
		if (amount > senderWallet.balance) return new Error(`Amount: ${amount} exceeds balance.`)
		return null
	}

	static newTransaction(senderWallet, recipient, amount, fee) {
		let hasErrors = Transaction.hasTransactionInformationErrors(senderWallet, recipient, amount, fee)
		if (hasErrors) throw hasErrors
		
		return Transaction.transactionWithOutputs(senderWallet, [
			{amount: senderWallet.balance - amount, address: senderWallet.publicKey,},
			{amount: amount - fee, address: recipient, fee: fee, },
		], fee)
	}

	static rewardTransaction(minerWallet, blockchainWallet, transactions) {
		let totalFee = 0
        
		transactions.forEach(t => {
			totalFee += t.outputs.reduce((total, output) => {
				let fee = output.fee ? output.fee : 0
				return total + fee
			}, 0)
		})
        
		return Transaction.transactionWithOutputs(blockchainWallet, [
			{ amount: totalFee, address: minerWallet.publicKey, },
		])
	}

	static signTransaction(transaction, senderWallet) {
		transaction.input = {
			timestamp: Date.now(),
			amount: senderWallet.balance,
			address: senderWallet.publicKey,
			signature: senderWallet.sign(ChainUtil.hash(transaction.outputs)),
		}
	}
    
	static verifyTransaction(transaction) {
		return ChainUtil.verifySignature(
			transaction.input.address,
			transaction.input.signature,
			ChainUtil.hash(transaction.outputs)
		)
	}
}

module.exports = Transaction