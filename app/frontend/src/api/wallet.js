import axios from "axios";

// const mock_balance = 200;

export default {
  getBalance(publicKey, cb) {
    axios
      .get("http://127.0.0.1:3001/wallet/balance")
      .then(cb)
      .catch(cb);
    // setTimeout(() => cb(mock_balance), 100);
  },

  getAddress(cb) {
    axios
      .get("http://127.0.0.1:3001/wallet/public-key")
      .then(cb)
      .catch(cb);
    // setTimeout(() => cb(mock_balance), 100);
  },

  getTransactions(cb) {
    axios
      .get("http://localhost:3001/transactions")
      .then(cb)
      .catch(cb);
  },

  createTransaction(recipient, cb) {
    axios
      .post("http://localhost:3001/transactions", recipient)
      .then(cb)
      .catch(cb);
  },

  mine(cb) {
    axios
      .get("http://localhost:3001/mine_transactions")
      .then(cb)
      .catch(cb);
  }
};
