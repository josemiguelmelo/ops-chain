export default {
  walletAddress(publicKey) {
    if (publicKey == null) return "";

    return publicKey.substring(0, 12);
  }
};
