import Vue from "vue";
import Router from "vue-router";
import Wallet from "./views/Wallet.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "wallet",
      component: Wallet
    }
  ]
});
