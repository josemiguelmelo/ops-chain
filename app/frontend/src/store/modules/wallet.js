import wallet from "@/api/wallet";

const state = {
  publicKey: null,
  privateKey: null,
  transactions: [],
  balance: 0,
  open: false
};

const getters = {
  keyCombination: state => {
    return { public: state.publicKey, private: state.privateKey };
  },

  balance: state => {
    return state.balance;
  },

  isOpen: state => {
    return state.open;
  },

  transactions: state => {
    return state.transactions;
  }
};

const actions = {
  mine({ commit, state }) {
    wallet.mine(() => {
      actions.getWalletBalance({ commit, state });
      actions.getTransactions({ commit });
    });
  },

  getWalletBalance({ commit, state }) {
    wallet.getBalance(state.publicKey, balanceResponse => {
      commit("UPDATE_BALANCE", balanceResponse.data.balance);
    });
  },

  getAddress({ commit }) {
    wallet.getAddress(response => {
      commit("UPDATE_PUBLIC_KEY", response.data.publicKey);
    });
  },

  openWallet({ commit }, publicKey) {
    commit("CHANGE_WALLET", publicKey);
  },

  closeWallet({ commit }) {
    commit("CLOSE_WALLET");
  },

  createTransaction({ commit }, recipient) {
    wallet.createTransaction(recipient, response => {
      commit("TRANSACTION_UPDATED", response.data);
    });
  },

  getTransactions({ commit }) {
    wallet.getTransactions(response => {
      commit("TRANSACTION_UPDATED", response.data);
    });
  }
};

const mutations = {
  setPrivateKey(state, { privateKey }) {
    state.privateKey = privateKey;
  },

  UPDATE_BALANCE(state, balance) {
    state.balance = balance;
  },

  UPDATE_PUBLIC_KEY(state, publicKey) {
    state.publicKey = publicKey;
  },

  CHANGE_WALLET(state, publicKey) {
    state.publicKey = publicKey;
    state.privateKey = null;
    state.open = true;
  },

  CLOSE_WALLET(state) {
    state.publicKey = null;
    state.privateKey = null;
    state.open = false;
  },

  TRANSACTION_UPDATED(state, transactions) {
    state.transactions = transactions;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
