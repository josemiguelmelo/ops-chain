import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store/index";
import BootstrapVue from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

Vue.use(BootstrapVue);
Vue.config.productionTip = false;

import NavBar from "@/components/infrastructure/NavBar.vue";
import OpenWalletForm from "@/components/wallet/OpenWalletForm.vue";
import CreateTransactionForm from "@/components/wallet/CreateTransactionForm.vue";
import TransactionPool from "@/components/wallet/TransactionPool.vue";

Vue.component("navBar", NavBar);
Vue.component("openWalletForm", OpenWalletForm);
Vue.component("createTransactionForm", CreateTransactionForm);
Vue.component("transactionPool", TransactionPool);

import walletUtils from "@/utils/wallet";

Vue.prototype.$utils = {
  wallet: walletUtils
};

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
