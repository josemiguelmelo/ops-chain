const express = require('express')
const bodyParser = require('body-parser')
const Blockchain = require('../blockchain')
const P2pServer = require('./p2p-server')
const Wallet = require('../wallet')
const TransactionPool = require('../wallet/transaction-pool')
const Miner = require('../miner')

const HTTP_PORT = process.env.HTTP_PORT || 3001

const app = express()
const cors = require('cors')
const bc = new Blockchain()
const wallet = new Wallet()
const tp = new TransactionPool()
const p2pServer = new P2pServer(bc, tp)
const miner = new Miner(bc, tp, wallet, p2pServer)

console.log(wallet.toString())

app.use(bodyParser.json())

app.use(cors())
app.options('*', cors()) // include before other routes

// blocks related routes
require('./routes/blocks')(app, bc, p2pServer, miner)

// transactions related routes
require('./routes/transactions')(app, bc, p2pServer, miner, tp, wallet)

// wallet related routes
require('./routes/wallet')(app, bc, p2pServer, miner, tp, wallet)


app.listen(HTTP_PORT, () => console.log(`Listening on port ${HTTP_PORT}`))
p2pServer.listen()