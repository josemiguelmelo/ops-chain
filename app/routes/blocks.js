module.exports = function(app, bc, p2pServer, miner) {
	app.get('/blocks', (req, res) => {
		return res.json(bc.chain).status(200)
	})

	app.post('/blocks/mine', (req, res) => {
		const block = bc.addBlock(req.body.data)
		console.log(`New block added ${block.toString()}`)
        
		p2pServer.syncChains()

		return res.redirect('/blocks')
	})

	app.get('/mine_transactions', (req, res) => {
		const block = miner.mine()
		console.log(`New block added: ${block.toString()}`)
		return res.redirect('/blocks')
	})
}