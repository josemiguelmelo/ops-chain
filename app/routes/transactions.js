module.exports = function(app, bc, p2pServer, miner, tp, wallet) {
	app.get('/transactions', (req, res) => {
		return res.json(tp.transactions).status(200)
	})

	app.post('/transactions', (req, res) => {
		const { recipient, amount, fee, } = req.body
        
		try{
			const transaction = wallet.createTransaction(recipient, parseFloat(amount), bc, tp, parseFloat(fee))
			p2pServer.broadcastTransaction(transaction)
			return res.redirect('/transactions')
		} catch (e) {
			res.json({error : e.message,}).status(400)
		}
	})
}