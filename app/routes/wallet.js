module.exports = function(app, bc, p2pServer, miner, tp, wallet) {
	app.get('/wallet/public-key', (req, res) => {
		res.json({ publicKey: wallet.publicKey, })
	})
    
	app.get('/wallet/balance', (req, res) => {
		res.json({ wallet: wallet.publicKey, balance: wallet.calculateBalance(bc), })
	})
}