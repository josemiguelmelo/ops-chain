const Blockchain = require('./index')
const Block = require('./block')

describe('Blockchain Test', () => {
	let blockchain, blockchain2

	beforeEach(() => {
		blockchain = new Blockchain()
		blockchain2 = new Blockchain()
	})

	it('starts with genesis block', () => {
		expect(blockchain.chain[0]).toEqual(Block.genesis())
	})
    
	it('adds new block', () => {
		const data = 'foo'
		blockchain.addBlock(data)

		expect(blockchain.chain[blockchain.chain.length - 1].data).toEqual(data)
	})

	it('validates a valid chain', () => {
		blockchain2.addBlock('foo')
        
		expect(blockchain.isValidChain(blockchain2.chain)).toBe(true)

	})

	it('invalidates a chain with corrupt genesis block', () => {
		blockchain2.chain[0].data = 'corrupt'

		expect(blockchain.isValidChain(blockchain2.chain)).toBe(false)
	})

	it('invalidates a corrupt chain', () => {
		blockchain2.addBlock('foo')
		blockchain2.chain[1].data = 'corrupt'

		expect(blockchain.isValidChain(blockchain2.chain)).toBe(false)
	})

	it('replace chain with a valid new chain', () => {
		blockchain2.addBlock('foo')
		blockchain.replaceChain(blockchain2.chain)
        
		expect(blockchain.chain).toEqual(blockchain2.chain)
	})

	it('it does not replace the chain with one of less than or equal to length', () => {
		blockchain.addBlock('foo')
		blockchain.replaceChain(blockchain2.chain)
        
		expect(blockchain.chain).not.toEqual(blockchain2.chain)
	})

	it('it does not replace the chain with one with invalid chain', () => {
		blockchain2.addBlock('foo')
		blockchain2.chain[blockchain2.chain.length - 1].data = 'corrupt'
		blockchain.replaceChain(blockchain2.chain)
        
		expect(blockchain.chain).not.toEqual(blockchain2.chain)
	})
})